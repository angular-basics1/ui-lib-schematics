import { mergeWith, apply, chain, Rule, applyTemplates, move, url, Tree } from '@angular-devkit/schematics';
import {AddComponentsType} from "./add-components-type";
import {strings, normalize} from "@angular-devkit/core";
import {addRouteDeclarationToModule} from '@schematics/angular/utility/ast-utils';
import * as ts from 'typescript';
import {InsertChange} from '@schematics/angular/utility/change';

export function AddComponents(_options: AddComponentsType): Rule {
  return () => {
    const templateResource = apply(
      url('./files/module'), [
        applyTemplates({
          dasherize: strings.dasherize,
          classify: strings.classify,
          name: _options.name
        }),
        move(normalize(`/${_options.path}`))
      ]
    );
    const templateResourceAssets = apply(
      url('./files/assets'), [
        applyTemplates({
          dasherize: strings.dasherize,
          classify: strings.classify,
          name: _options.name
        }),
        move(normalize(`/src/assets/i18n`))
      ]
    );
    return chain([addRouteDeclarationToNgModule(),
      mergeWith(templateResource), mergeWith(templateResourceAssets)
    ])
  };

  function addRouteDeclarationToNgModule(): Rule {
    return (tree: Tree) => {

      let path = './src/app/app-routing.module.ts';
      const sourceText = tree.readText(path);

      const index = _options.path.indexOf("src/app/");
      const length = "src/app/".length;
      const result = _options.path.slice(index + length);


      const addDeclaration = addRouteDeclarationToModule(ts.createSourceFile(path, sourceText, ts.ScriptTarget.Latest, true), './src/app',
        `
      {
    canActivate: [AuthGuard],
    path: '${strings.dasherize(_options.name)}',
    loadChildren: () => import('./${result}/${strings.dasherize(_options.name)}/${strings.dasherize(_options.name)}.module').then(m => m.${strings.classify(_options.name)}Module)
  }
      `) as InsertChange;
      const recorder = tree.beginUpdate(path);
      recorder.insertLeft(addDeclaration.pos, addDeclaration.toAdd);
      tree.commitUpdate(recorder);
      return tree;
    };
  }
}

